import pygame 
import os 
import xml.dom.minidom 

print pygame.font.get_fonts()

WORDS = [
("","Jake Parker"),
("","Peter Lu"),
("","Simon Wiscombe"),
("","Topher Florence"),

]   

WORDS = [
("","CONTINUE 9"),
("","CONTINUE 9"),
("","CONTINUE 9"),
("","CONTINUE 9"),
("","CONTINUE 9"),
("","CONTINUE 9"),
("","CONTINUE 8"),
("","CONTINUE 8"),
("","CONTINUE 8"),
("","CONTINUE 8"),
("","CONTINUE 8"),
("","CONTINUE 8"),
("","CONTINUE 7"),
("","CONTINUE 7"),
("","CONTINUE 7"),
("","CONTINUE 7"),
("","CONTINUE 7"),
("","CONTINUE 7"),
("","CONTINUE 6"),
("","CONTINUE 6"),
("","CONTINUE 6"),
("","CONTINUE 6"),
("","CONTINUE 6"),
("","CONTINUE 6"),
("","CONTINUE 5"),
("","CONTINUE 5"),
("","CONTINUE 5"),
("","CONTINUE 5"),
("","CONTINUE 5"),
("","CONTINUE 5"),
("","CONTINUE 4"),
("","CONTINUE 4"),
("","CONTINUE 4"),
("","CONTINUE 4"),
("","CONTINUE 4"),
("","CONTINUE 4"),
("","CONTINUE 3"),
("","CONTINUE 3"),
("","CONTINUE 3"),
("","CONTINUE 3"),
("","CONTINUE 3"),
("","CONTINUE 3"),
("","CONTINUE 2"),
("","CONTINUE 2"),
("","CONTINUE 2"),
("","CONTINUE 2"),
("","CONTINUE 2"),
("","CONTINUE 2"),
("","CONTINUE 1"),
("","CONTINUE 1"),
("","CONTINUE 1"),
("","CONTINUE 1"),
("","CONTINUE 1"),
("","CONTINUE 1"),
("","CONTINUE  "),
("",""),
("","CONTINUE  "), 
("",""),
("","CONTINUE  "), 
("",""),
("","CONTINUE  "), 
("",""),
("","CONTINUE  "), 
("",""),
("","GAME OVER"),
]
PATH = "credits/"
SIZE = (320,240)
FONT_SIZE = 30
FONT_NAME = u"almohanad"
FONT_NAME = u'dejavusans'
FONT_NAME = u'vlgothic'
pygame.init()
screen = pygame.display.set_mode(SIZE,pygame.DOUBLEBUF)
font = pygame.font.SysFont(FONT_NAME,FONT_SIZE)
counter = 0
for i,j in WORDS:
	screen.fill((0,0,0))
	t = font.render(j,False,(255,255,255))
	screen.blit(t,(SIZE[0]/2 - t.get_width()/2,SIZE[1]/2 - t.get_height()/2))
	pygame.display.flip()
	pygame.image.save(screen,os.path.join(PATH,"credits"+str(counter).zfill(5)+".png"))
	counter += 1
